# Gin Admin


## 简介
Gin Admin 是一个用 Gin 框架开发的后台管理系统。前端包含Layui、Vue2+Element、Vue3+Arco-Design等版本。

[![star](https://gitee.com/hchen2017/golang-admin/badge/star.svg?theme=dark)](https://gitee.com/hchen2017/golang-admin/stargazers)
[![fork](https://gitee.com/hchen2017/golang-admin/badge/fork.svg?theme=dark)](https://gitee.com/hchen2017/golang-admin/members)
[![License](https://img.shields.io/badge/license-Apache2-yellow)](https://gitee.com/hchen2017/golang-admin/blob/master/LICENSE)


## 功能
* 用户管理
* 权限管理
* 会员管理
* 内容管理
* 菜单管理
* 文件管理
* 系统管理
* 数据库管理


## 安装
### 前端开发
[adminUI](https://gitee.com/hchen2017/admin-ui)

### 后端安装
- Windows 安装
~~~
1.下载并安装 gin：
$ go get -u github.com/gin-gonic/gin
2.将 gin 引入到代码中：
import "github.com/gin-gonic/gin"
3.初始化 go mod
$ go mod init project_name
4.启动项目
$ go run main.go
~~~
- 包
~~~
图像验证码
go get -u github.com/mojocn/base64Captcha
~~~


## 项目截图

### Layui
- ![article](./public/static/img/layui/article.png)

### Element
- ![index](./public/static/img/element/index.png)
- ![article](./public/static/img/element/article.png)
- ![menu](./public/static/img/element/menu.png)
- ![database](./public/static/img/element/database.png)

### Arco Design
- ![login](./public/static/img/arco-design/login.png)
- ![slide-verify](./public/static/img/arco-design/slide-verify.png)
- ![index](./public/static/img/arco-design/index.png)
- ![role](./public/static/img/arco-design/role.png)
- ![admin](./public/static/img/arco-design/admin.png)
- ![article](./public/static/img/arco-design/article.png)
- ![menu](./public/static/img/arco-design/menu.png)
- ![file](./public/static/img/arco-design/file.png)
- ![region](./public/static/img/arco-design/region.png)
- ![database](./public/static/img/arco-design/database.png)
- ![system](./public/static/img/arco-design/system.png)
- ![test](./public/static/img/arco-design/test.png)


## 交流
交流QQ群：614159657


## 💐 特别鸣谢
- 👉 Gin：[https://gin-gonic.com](https://gin-gonic.com)
- 👉 Element UI：[https://element.eleme.io](https://element.eleme.io)
- 👉 Arco Design：[https://arco.design/vue/docs/pro/start](https://arco.design/vue/docs/pro/start)

如果对您有帮助，您可以点右上角 💘Star💘支持一下，谢谢！！！
