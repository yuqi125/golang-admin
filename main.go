package main

import (
  	//"net/http"

  	//"github.com/gin-gonic/gin"

	. "mygin/routes"
)

// 通过字典模拟 DB
var db = make(map[string]string)

func main() {
	// 设置路由信息
	router := SetupRouter()
	// 启动服务器并监听 8080 端口
	router.Run(":9090")
}
