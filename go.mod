module mygin

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/mojocn/base64Captcha v1.3.5
	gorm.io/driver/mysql v1.2.3
	gorm.io/gorm v1.22.5
)
