package routes

import (
    "net/http"
    "log"
    "time"

  	"github.com/gin-gonic/gin"

    "mygin/app/controller/admin"
    "mygin/app/controller/home"
)

func Logger() gin.HandlerFunc {
    return func(c *gin.Context) {
        t := time.Now()

        // 设置 example 变量
        c.Set("example", "12345")

        // 请求之前...

        c.Next()

        // 请求之后...

        latency := time.Since(t)
        // 打印请求处理时间
        log.Print(latency)

        // 访问即将发送的响应状态码
        status := c.Writer.Status()
        log.Println(status)
    }
}

func SetupRouter() *gin.Engine {
    // 初始化 Gin 框架默认实例，该实例包含了路由、中间件以及配置信息
    //router := gin.Default()
    // 创建一个默认不使用任何中间件的路由器
    router := gin.New()
    // 使用自定义的 Logger 全局中间件
    //router.Use(Logger())

    // Ping 测试路由
    router.GET("/ping", func(c *gin.Context) {
        c.String(http.StatusOK, "pong")
    })


    // ++++++++++++++++++++ Web前端 ++++++++++++++++++++ //
    router.GET("/", func(c *gin.Context) {
        // HTML 视图模板
        // router.LoadHTMLGlob("views/**/**/*")
        // //router.LoadHTMLFiles("views/template1.html", "templates/template2.html")
        // c.HTML(http.StatusOK, "home/index/index.html", gin.H {
        //     "title": "Main website",
        // })

        home.Index(c)
    })


    // ++++++++++++++++++++ 后台管理 ++++++++++++++++++++ //
    router.GET("/admin", func(c *gin.Context) {
        admin.Index(c)
    })

    router_admin := router.Group("/admin")
	{
		router_admin.GET("/auth/login", func(c *gin.Context) {
            admin.Login(c)
        })
        router_admin.POST("/auth/dologin", func(c *gin.Context) {
            admin.DoLogin(c)
        })
        router_admin.GET("/auth/logout", func(c *gin.Context) {
            admin.Logout(c)
        })
	}

    router.GET("/admin/dashboard/index", func(c *gin.Context) {
        admin.Index(c)
    })
    router.GET("/admin/admin/index", func(c *gin.Context) {
        admin.AdminIndex(c)
    })
    router.GET("/admin/admin/profile", func(c *gin.Context) {
        admin.AdminProfile(c)
    })
    router.GET("/admin/admin/resetpassword", func(c *gin.Context) {
        admin.AdminResetPassword(c)
    })
    router.GET("/admin/admin/add", func(c *gin.Context) {
        admin.AdminAdd(c)
    })
    router.GET("/admin/admin/edit", func(c *gin.Context) {
        admin.AdminEdit(c)
    })
    router.GET("/admin/admin/savepost", func(c *gin.Context) {
        admin.AdminSavePost(c)
    })
    router.GET("/admin/admin/delete", func(c *gin.Context) {
        admin.AdminDelete(c)
    })
    router.GET("/admin/admin/checkloginname", func(c *gin.Context) {
        admin.AdminCheckLoginName(c)
    })

    router.GET("/admin/captcha", func(c *gin.Context) {
        admin.Captcha(c)
    })

    // // 获取用户数据路由
    // router.GET("/user/:name", func(c *gin.Context) {
    //     user := c.Params.ByName("name")
    //     value, ok := db[user]
    //     if ok {
    //         c.JSON(http.StatusOK, gin.H{"user": user, "value": value})
    //     } else {
    //         c.JSON(http.StatusOK, gin.H{"user": user, "status": "no value"})
    //     }
    // })

    // 需要 HTTP 基本授权认证的子路由群组设置
    // authorized := router.Group("/", gin.BasicAuth(gin.Accounts{
    //     "foo":  "bar", // 用户名:foo 密码:bar
    //     "manu": "123", // 用户名:manu 密码:123
    // }))

    // 保存用户信息路由
    // authorized.POST("admin", func(c *gin.Context) {
    //     user := c.MustGet(gin.AuthUserKey).(string)

    //     // 解析并验证 JSON 格式请求数据
    //     var json struct {
    //     Value string `json:"value" binding:"required"`
    //     }

    //     if c.Bind(&json) == nil {
    //         db[user] = json.Value
    //         c.JSON(http.StatusOK, gin.H{"status": "ok"})
    //     }
    // })


    // 静态资源
    //router.Static("/assets", "public/static")
    router.Static("/static", "public/static")
    router.Static("/public", "public")
    router.StaticFS("/uploads", http.Dir("public/uploads"))
    router.StaticFile("/favicon.ico", "./public/favicon.ico")

    return router
}
