package models

import (
	"gorm.io/gorm"
	"gorm.io/driver/mysql"
	"log"
)

var DB *gorm.DB // 全局变量，这样可以在别处调用

// init 方法在 Web 应用启动时自动初始化数据库连接
func init() {
	log.Println("Init Mysql Begin")
	var err error

	dsn := "root:root@tcp(127.0.0.1:3306)/golang?charset=utf8mb4&parseTime=True&loc=Local" // 本地测试
	//dsn := "root:root_Erp2021SLkf!*-@tcp(127.0.0.1:3306)/golang?charset=utf8mb4&parseTime=True&loc=Local"
    DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
    if err != nil {
        log.Fatal(err)
	}

	log.Println("Init Mysql End")
}
