package models

import (
	//. "mygin/app/helpers"
)

type Admin struct {
	Id int `json:"id"`
	//CreatedAt time.Time // 2020-05-20 10:10:10
	CreatedAt int `json:"created_at"`
	Username string `json:"username"`
	Password string `json:"password"`
	Email string `json:"email"`
	Avatar string `json:"avatar"`
	AutoLogout int `json:"auto_logout"`
	Status int `json:"status"`
}

func SelectAdmins(sql string, args ... interface{}) (admins []Admin, err error) {
	rows, err := DB.Raw(sql, args...).Rows()
	defer rows.Close()
	for rows.Next() {
		conv := Admin{}
		rows.Scan(&conv.Id, &conv.CreatedAt, &conv.Username, &conv.Password, &conv.Email, &conv.Avatar, &conv.Status);
	}

	return
}

func SelectAdminById(id int) (admin Admin, err error) {
	result := DB.Table("admins").Where("id = ?", id).Find(&admin)
	err = result.Error

	return
}

// 通过姓名查找
func AdminByUsername(username string) (admin Admin, err error) {
	result := DB.Table("admins").Where("username = ?", username).First(&admin)
	err = result.Error

    return
}

// Create
func CreateAdmin() {
	// var r Admin
	// if err := c.Bind(&r); err != nil {
	// 	return
	// }
	// u := Admin {
	// 	Username: r.Username,
	// 	Password: r.Password,
	// }

	// if _, err := u.Create(); err != nil {
	// 	return
	// }
}

// Update
func (admin *Admin) UpdateAdmin() (err error) {

    return
}
