package home

import (
    //"net/http"
    "html/template"
    
    "github.com/gin-gonic/gin"
)

// 首页路由处理器方法
func Index(c *gin.Context) {
    files := []string{"views/home/index/index.html", "views/home/layout/layout.html", "views/home/public/header.html", "views/home/public/sidebar.html", "views/home/public/footer.html"}
    templates := template.Must(template.ParseFiles(files...))
    
    // 从 layout.html 开始解析模板
    templates.ExecuteTemplate(c.Writer, "home/layout/layout", "welcome index")
}