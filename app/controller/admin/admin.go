package admin

import (
    //"net/http"

    "github.com/gin-gonic/gin"
)

// List
func AdminIndex(c *gin.Context) {
    Initialize(c)
}

func AdminProfile(c *gin.Context) {
    Initialize(c)
}

func AdminResetPassword(c *gin.Context) {
    Initialize(c)
}

func AdminAdd(c *gin.Context) {
    Initialize(c)
}

func AdminEdit(c *gin.Context) {
    Initialize(c)
}

func AdminSavePost(c *gin.Context) {
    Initialize(c)
}

func AdminDelete(c *gin.Context) {
    //id := c.Query("id")
    Initialize(c)
}

func AdminCheckLoginName(c *gin.Context) {
    Initialize(c)
}
