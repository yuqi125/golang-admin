package admin

import (
    "net/http"
    //"fmt"
    //"io"
    //"encoding/json"
    //"log"

    "github.com/gin-gonic/gin"

    "mygin/app/services"
)

// 验证码
func Captcha(c *gin.Context) {
	// 接收客户端发送来的请求参数
    // decoder := json.NewDecoder(r.Body)

    // var param ConfigJsonBody
    // err := decoder.Decode(&param)
    // if err != nil {
    //     log.Println(err)
    // }
    // defer r.Body.Close()
    //length := c.Param("length")
    //height := c.Param("height")

    dada, err := services.CaptchaEntry()
    if err != nil {
        c.JSON(http.StatusOK, gin.H {
    		"code": 200,
    		"status": 0,
    		"msg": err.Error(),
    	})
    	return
    }
	c.JSON(http.StatusOK, gin.H {
	    "code": http.StatusOK,
		"status": 1,
		"data": dada,
		"msg": "success",
	})
}
