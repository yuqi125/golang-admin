layui.use(['form', 'layer', 'table', 'laydate', 'layedit'], function () {

    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        layedit = layui.layedit,
        table = layui.table;

    var datalist_url = $('.datalist_url').val();
    var add_url = $('.add_url').val();
    var edit_url = $('.edit_url').val();
    var delete_url = $('.delete_url').val();
    var save_url = $('.save_url').val();

    var restore_url = $('.restore_url').val();
    var clean_url = $('.clean_url').val();


    // 日期
    laydate.render({
        elem: '.lay-date-range',
        type: 'datetime',
        range: '~', // '~' 自定义分割字符
        format: 'yyyy-MM-dd HH:mm:ss',
    });

    // 富文本编辑器
    layedit.set({
        // 图片上传接口
        uploadImage: {
            url: UPLOAD_URL + '?method=layui&folder=article', // 接口url
            type: 'post', // 默认post
        }
    });
    // 建立编辑器 layedit.set 一定要放在 build 前面，否则配置全局接口将无效
    var content = layedit.build('content');


    // Table 列表
    var tableIns = table.render({
        elem: '#dataTable',
        url: datalist_url,
        page: true,
        cellMinWidth: 95,
        //height : "full-100",
        limit: 10,
        limits: [10, 20, 50, 100],
        id: "tableList",
        cols: [[
            {field: 'id', title: '序号', width: 80, align: "center"},
            {field: 'title', title: '标题', minWidth: 180, align: "center"},
            {field: 'keywords', title: '关键字', minWidth: 180, align: "center"},
            {field: 'excerpt', title: '摘要', minWidth: 180, align: "center"},
            {field: 'created_at', title: '创建时间', minWidth: 160, align: "center"},
            {
                title: '操作', fixed: "right", align: "center", minWidth: 160, templet: function (data) {
                    var html = '';
                    if (data.status != 90) {
                        html += '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>';
                        html += '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del"><i class="layui-icon layui-icon-delete"></i>删除</a>';
                    } else {
                        // 回收站
                        html += '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="restore"><i class="layui-icon layui-icon-return"></i>还原</a>';
                        html += '<a class="layui-btn layui-btn-xs layui-btn-warm" lay-event="clean"><i class="layui-icon layui-icon-delete"></i>彻底删除</a>';
                    }

                    return html;
                }
            }
        ]]
    });

    // 搜索
    $(".search_btn").on("click", function () {
        var title = $('.searchBox').find('input[name=title]').val();
        var range_date = $('.searchBox').find('input[name=range_date]').val();
        if (title != '' || range_date != '') {
            table.reload("tableList", {
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                    title: title,
                    range_date: range_date
                }
            });
        } else {
            layer.msg("请选择筛选条件");
        }
    });

    // 清空搜索
    $(".reset_btn").on("click", function () {
        table.reload("tableList", {
            where: {
                title: '',
                range_date: ''
            }
        });
    });

    $('.add_btn').on('click', function () {
        formHtml();
    });

    // 列表操作
    table.on('tool(dataTable)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        // 删除
        if (layEvent === 'del') {
            layer.confirm('确定删除此数据？', {icon: 3, title: '提示信息'}, function (index) {
                $.ajax({
                    url: delete_url,
                    data: {"id": data.id},
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.code == 1) {
                            layer.msg('已删除', {icon: 1, time: 1000});
                            tableIns.reload();
                            layer.close(index);
                        } else {
                            layer.msg(res.msg);
                        }
                    },
                    error: function (data) {
                        layer.msg("服务器无响应");
                    }
                });
            });
        } else if (layEvent === 'edit') {
            formHtml(data);
        } else if (layEvent === 'restore') {
            // 还原
            layer.confirm('确定还原此数据？', {icon: 3, title: '提示信息'}, function (index) {
                $.ajax({
                    url: restore_url,
                    data: {"id": data.id},
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.code == 1) {
                            layer.msg('已还原', {icon: 1, time: 1000});
                            tableIns.reload();
                            layer.close(index);
                        } else {
                            layer.msg(res.msg);
                        }
                    },
                    error: function (data) {
                        layer.msg("服务器无响应");
                    }
                });
            });
        } else if (layEvent === 'clean') {
            // 彻底删除
            layer.confirm('确定彻底删除此数据？', {icon: 3, title: '提示信息'}, function (index) {
                $.ajax({
                    url: clean_url,
                    data: {"id": data.id},
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.code == 1) {
                            layer.msg('已彻底删除', {icon: 1, time: 1000});
                            tableIns.reload();
                            layer.close(index);
                        } else {
                            layer.msg(res.msg);
                        }
                    },
                    error: function (data) {
                        layer.msg("服务器无响应");
                    }
                });
            });
        }
    });

    // Form 表单
    function formHtml(data) {
        var content = add_url;

        if (data) {
            content = edit_url + '?id=' + data.id;
        }

        var index = layer.open({
            title: "文章管理",
            type: 2,
            area: ["800px", "600px"],
            content: content,
            success: function (layero, index) {
                form.render();
            }
        });
    }

    // 自定义验证规则
    form.verify({
        image: function (value) {
            if (value == '') {
                return '请上传图片';
            }
        },
    });

    // 表单提交
    form.on("submit(laySave)", function (data) {
        data.field.content = layedit.getContent(content);

        submitForm(data);
    });

    // 表单提交
    function submitForm(data) {
        // 弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        $.ajax({
            url: save_url,
            data: data.field,
            type: "post",
            dataType: "json",
            success: function (res) {
                layer.msg(res.msg);
                if (res.code == 1) {
                    layer.closeAll("iframe");
                    //parent.window.tableIns.reload();
                    parent.layui.table.reload('tableList', {});
                }
            },
            error: function (data) {
                layer.msg("服务器无响应");
            }
        });
    }
});